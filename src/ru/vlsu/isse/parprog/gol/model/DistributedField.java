/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.vlsu.isse.parprog.gol.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;

/**
 *
 * @author gerasimov.pk
 */
public class DistributedField extends ClosedField {

    private static final int overlap = 1;
    private static final int headerSize = Integer.SIZE * 2 / 8;

    public DistributedField(Field field) {
        super(field);
    }

    private DistributedField(int width, int height) {
        super(width, height);
    }

    public byte[] getRow(int index) {
        byte[] row = new byte[width / 8];
        System.arraycopy(cells, index * width / 8, row, 0, row.length);
        return row;
    }

    public void setRow(byte[] row, int index) {
        System.arraycopy(row, 0, cells, width * index / 8, row.length);
    }

    public void saveToFile(File target, int firstLine, int lastLine, int part) throws IOException {
        int bytesInLine = width / 8;

        RandomAccessFile output = new RandomAccessFile(target, "rw");
        int startPosition = (height - overlap * 2) * part * bytesInLine + headerSize;
        try {
            output.seek(startPosition);
            output.write(cells, firstLine * bytesInLine, (lastLine - firstLine - overlap) * bytesInLine);
        } finally {
            output.close();
        }
    }

    
    public void saveHeader(File target, int size) throws IOException {
        OutputStream output = new FileOutputStream(target);
        try {
            writeInt(output, width);
            writeInt(output, (height - overlap * 2) * size);
        } finally {
            output.close();
        }
    }

    public static Field partialLoadFromFile(File source, int chunkNumber, int totalCount) throws IOException {
        RandomAccessFile input = new RandomAccessFile(source, "r");
        try {
            int width = readInt(input);
            int height = readInt(input);
            int chunkSize = height / totalCount;
            if (chunkNumber == totalCount - 1) {
                chunkSize = height - (chunkSize * (totalCount - 1));
            }
            DistributedField field = new DistributedField(width, chunkSize + overlap * 2);

            int bytesInLine = width / 8;
            
            int offset = chunkSize * chunkNumber - overlap;
            int readFromStart = 0;
            if (offset < 0) {
                input.seek(input.length() + offset * bytesInLine);
                input.read(field.cells, 0, -offset * bytesInLine);
                readFromStart = -offset;
                offset = 0;
            }
            int length = chunkSize + overlap * 2 - readFromStart;
            if (offset + length > height) {
                int endOverlap = offset + length - height;
                input.seek(headerSize);
                input.read(field.cells, (chunkSize - endOverlap + overlap + 1) * bytesInLine, endOverlap * bytesInLine);
                length = length - endOverlap;
            }

            input.seek((offset) * bytesInLine + headerSize);
            input.read(field.cells, readFromStart * bytesInLine, length * bytesInLine);

            return field;
        } finally {
            input.close();
        }
    }

    private void writeInt(OutputStream output, int value) throws IOException {
        for (int i = 0; i < 4; i++) {
            output.write(value);
            value >>>= 8;
        }
    }

    private static int readInt(RandomAccessFile input) throws IOException {
        int value = 0;
        for (int i = 0; i < 4; i++) {
            int nextByte = input.read();
            nextByte <<= i * 8;
            value |= nextByte;
        }
        return value;
    }

}
