/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.vlsu.isse.parprog.gol;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import mpi.MPI;
import ru.vlsu.isse.parprog.gol.model.ClosedField;
import ru.vlsu.isse.parprog.gol.model.DistributedField;
import ru.vlsu.isse.parprog.gol.model.Field;

/**
 *
 * @author gerasimov.pk
 */
public class Main {

    private final static String INPUT_FILE = "C:/temp/GoL/generated.dat";
    private final static String OUTUT_FILE = "C:/temp/GoL/output_d.dat";
    private final static int NUMBER_OF_EPOCHS = 5;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] mpiargs) throws IOException {
        MPI.Init(mpiargs);

        int me = MPI.COMM_WORLD.Rank();
        System.err.println("I am" + me);
        int size = MPI.COMM_WORLD.Size();

        DistributedField chunk = new DistributedField(DistributedField.partialLoadFromFile(new File(INPUT_FILE), me, size));
        Game game = new Game(chunk);
        
        File outputFile = new File(OUTUT_FILE);
        if (me == 0) {
            ((DistributedField)game.getField()).saveHeader(outputFile, size);
        }
        
        int prev = me - 1 >= 0 ? me - 1 : size - 1;
        int next = me + 1 < size ? me + 1 : 0;
        
        long time = System.currentTimeMillis();

        for (int i = 0; i < NUMBER_OF_EPOCHS; i++) {
            System.out.println("Started epoch: " + i);
            game.update();
            if (me % 2 != 0) {  
                sendData(game.getField(), prev, next);
                getData(game.getField(), prev, next);
            } else { 
                getData(game.getField(), prev, next);
                sendData(game.getField(), prev, next);
            }
        }
        System.out.println("Total time: " + (System.currentTimeMillis() - time));

        ((DistributedField)game.getField()).saveToFile(outputFile, 1, game.getField().height(), me);
        MPI.Finalize();
    }

    private static void getData(DistributedField cf, int prev, int next) {
        byte[] row = new byte[cf.width() / 8];
        MPI.COMM_WORLD.Recv(row, 0, row.length, MPI.BYTE, next, 0);
        cf.setRow(row, cf.height() - 1);

        MPI.COMM_WORLD.Recv(row, 0, row.length, MPI.BYTE, prev, 1);
        cf.setRow(row, 0);
    }

    private static void sendData(DistributedField cf, int prev, int next) {
        byte[] row = cf.getRow(1);
        MPI.COMM_WORLD.Send(row, 0, row.length, MPI.BYTE, prev, 0);

        row = cf.getRow(cf.height() - 2);
        MPI.COMM_WORLD.Send(row, 0, row.length, MPI.BYTE, next, 1);
    }
    
}
