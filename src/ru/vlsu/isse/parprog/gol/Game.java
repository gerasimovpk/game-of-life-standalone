package ru.vlsu.isse.parprog.gol;

import ru.vlsu.isse.parprog.gol.model.ClosedField;
import ru.vlsu.isse.parprog.gol.model.DistributedField;
import ru.vlsu.isse.parprog.gol.model.Field;

public class Game {

    private DistributedField activeField;
    private DistributedField bufferField;
    private boolean isActive;

    public Game(DistributedField field) {
        this.bufferField = field;
        this.activeField = new DistributedField(field);
        isActive = false;
    }

    public void update() {
        if (isActive) {
            updateField(activeField, bufferField, 0, bufferField.size());
        } else {
            updateField(bufferField, activeField, 0, bufferField.size());
        }
        isActive = !isActive;
    }

    public DistributedField getField() {
        return !isActive ? bufferField : activeField;
    }

    public void updateField(final DistributedField source, final Field dest, final int offset, final int length) {
        //from second to prelast line
        for (int position = offset + source.width(); position < offset + length - source.width(); position++) {
            int x = position / source.width();
            int y = position - source.width() * x;

            int neighbours = (source.isAlive(x - 1, y - 1) ? 1 : 0)
                    + (source.isAlive(x - 1, y + 0) ? 1 : 0)
                    + (source.isAlive(x - 1, y + 1) ? 1 : 0)
                    + (source.isAlive(x + 0, y - 1) ? 1 : 0)
                    + (source.isAlive(x + 0, y + 1) ? 1 : 0)
                    + (source.isAlive(x + 1, y - 1) ? 1 : 0)
                    + (source.isAlive(x + 1, y + 0) ? 1 : 0)
                    + (source.isAlive(x + 1, y + 1) ? 1 : 0);

            if (neighbours == 3) {
                dest.set(x, y, true);
            } else if (neighbours < 2 || neighbours > 3) {
                dest.set(x, y, false);
            } else {
                dest.set(x, y, source.isAlive(x, y));
            }
        }
    }
}
